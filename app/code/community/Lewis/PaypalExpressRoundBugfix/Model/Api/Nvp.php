<?php

class Lewis_PaypalExpressRoundBugfix_Model_Api_Nvp extends Mage_Paypal_Model_Api_Nvp {
	protected function _exportLineItems(array &$request, $i = 0) {
		parent::_exportLineItems($request, $i);
		$this->recalculateTotalsBeforeCall($request);
	}

	// Adjust shipping amount to prevent totals calculation triggering error on PayPal checkout
	protected function recalculateTotalsBeforeCall(&$request) {
		// Recalculate ITEMAMT from exact line totals to ensure match
		if ($this->getIsLineItemsEnabled()) {
			$i = 0;
			$newItemAmt = 0;
			while (isset( $request['L_AMT'.$i] )) {
				$newItemAmt += (float)$request['L_AMT'.$i];
				$i++;
			}
			$request['ITEMAMT'] = $newItemAmt;
		}

		// Calculate difference between AMT and other cart totals
		$trueAmt = (float)$request['ITEMAMT'] + (float)$request['TAXAMT'] + (float)$request['SHIPPINGAMT'];
		$diff = (float)$request['AMT']-$trueAmt;
		if( $diff != 0 ) {
			// Correct shipping amount by difference
			$request['SHIPPINGAMT'] = number_format((float)$request['SHIPPINGAMT']+$diff, 2, '.', '');
		}
	}
}
